import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import AppNavigator from './src/navigator/routes'

export default class App extends Component<Props> {
  render() {
    return <AppNavigator />
  }
}
