import React from 'react';
import {
  createAppContainer,
  createStackNavigator,
  StackActions,
  NavigationActions
} from 'react-navigation';

import LoginScreen from '../../screens/LoginScreen/scenes/login'

const AppNavigator = createStackNavigator({
  Home: {
    screen: LoginScreen,
    navigationOptions: {
    header: null
  }
  },
}, {
    initialRouteName: 'Home',
});

export default createAppContainer(AppNavigator);
