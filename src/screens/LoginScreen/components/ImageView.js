import React, {Component} from 'react'
import {Image, View} from 'react-native';

import TwitterLogo from '../../../utils/home__icon.png'
import styles from '../styles'

export default () => (
  <View style={styles.image}>
    <Image source={TwitterLogo} style={styles.logo} />
  </View>
)
