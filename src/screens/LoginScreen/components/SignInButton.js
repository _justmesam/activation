import React, {Component} from 'react'
import { TouchableOpacity, Text } from 'react-native';

import TwitterLogo from '../../../utils/home__icon.png'
import styles from '../styles'

export default (props) => (
  <TouchableOpacity
    onPress={()=> props.handleLogin()}
    style={styles.signinbuttonview}>
    <Text
     style={styles.signinbutton}
    >
    Sign In Via Twitter
    </Text>
  </TouchableOpacity>
)
