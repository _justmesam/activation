import React from 'react'
import {Text, View} from 'react-native';

import styles from '../styles'

export default () => (
  <View>
    <Text style={styles.welcome}>Welcome to Activation!!</Text>
    <Text style={styles.instructions}>Made for the influencers</Text>
  </View>
)
