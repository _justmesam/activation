import React, {Component} from 'react'
import {Text, View, NativeModules, AsyncStorage} from 'react-native';

import TextView from '../components/TextView'
import TwitterLogo from '../components/ImageView'
import SignInButton from '../components/SignInButton'
import styles from '../styles'

const { RNTwitterSignIn } = NativeModules

const Config = {
  TWITTER_CONSUMER_KEY: 'MeALByS0j0c6ifdwUbp9wfMJS',
  TWITTER_CONSUMER_SECRET_KEY: 'Nra99roNK0in4fnyEzEohXzK6kpLqbQBR9SoT8jlGaCl0yZgo9'
}

const getUserDetails = async () => {
  let isLoggedIn;
  try {
    isLoggedIn = await AsyncStorage.getItem('LOGGED_IN') || false
  } catch(error) {
    console.log(error.message);
  }
  return isLoggedIn
}

const saveUserDetail = async () => {
  try {
    await AsyncStorage.setItem('LOGGED_IN', true);
    return true
  } catch (error) {
    console.log(error.message);
    return false
  }
}

class LoginView  extends Component {
  state = {
    userIsLoggedIn: getUserDetails()
  }

  twitterSignin = () => {
    RNTwitterSignIn.init(
      Config.TWITTER_CONSUMER_KEY,
      Config.TWITTER_CONSUMER_SECRET_KEY
    )
    RNTwitterSignIn.logIn()
    .then(loginData => {
      if (loginData.authToken && loginData.authTokenSecret){
        saveUserDetail()
      }
    })
    .catch((error) => {
      console.log("<><><><>   ", error)
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <TextView />
        <TwitterLogo />
        <SignInButton
          handleLogin={this.twitterSignin}
         />
      </View>
    )
  }
}

export default LoginView
