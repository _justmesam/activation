import {Platform, StyleSheet, Text, View} from 'react-native';


export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1da1f2',
  },
  welcome: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#fff',
    marginBottom: 5,
  },
  logo: {
    height: 150,
    width: 150,
  },
  image: {
    margin: 20,
    height: 150,
    width: 150,
    borderRadius:80,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    ...Platform.select({
      android: {
        elevation: 10,
      }
    })
  },
  signinbuttonview:{
    justifyContent:'center',
    alignItems:'center',
    height:50,
    width:200,
    marginTop: 95,
    borderRadius: 30,
    borderWidth: 0.09,
    overflow:'hidden',
    backgroundColor: '#1b95e0',
  },
  signinbutton: {
    color: '#fff'
  }
});
